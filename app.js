const cors = require("cors");
const express = require("express");
const AppError = require("./utils/appError");
const errorHandler = require("./utils/errorHandler");
const cookieParser = require("cookie-parser");
var app = express();
const {signup} = require("./controllers/user/registrationController");
const {login} = require("./controllers/user/authController");
const { getFileFromS3 } = require("./controllers/common/getFileFromS3Controller");
const { transactionRoutes,reportRoutes } = require("./routes");


//serve static files
var publicDir = require("path").join(__dirname, "public");
app.use(express.static(publicDir));

app.use(
  cors({
    credentials: true,
    origin: [`${process.env.FRONTEND_URL}`], // here goes Frontend IP
  })
);


app.use(express.json());
app.use(cookieParser());

app.post("/signup", signup);

app.post("/login", login);

app.use("/transaction",transactionRoutes)

app.use("/report",reportRoutes);

//route to access files
app.get("/file/:key", getFileFromS3);


app.get('/', function (req, res) {
  res.send('Hello World!');
});

app.use(errorHandler);

module.exports = app;

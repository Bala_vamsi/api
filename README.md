
## To install all the dependencies
npm i 

## To Run Migrations
npx sequelize-cli db:migrate

## To start the server
npm start
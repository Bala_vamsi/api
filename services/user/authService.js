const { Op } = require("sequelize");
const jwt = require("jsonwebtoken");
const db = require("../../db/models");
const AppError = require("../../utils/appError");
const { signToken,signRefreshToken } = require("../common/jwtToken");
const { User } = db;
const bcrypt = require("bcryptjs");




exports.loginUser = async (credentials) => {
  let { userName,password } = credentials;
  
  // finding user based 
  const user = await User.findOne({ where: { userName } }); 
  const {status,remarks} = user;

  if (!user) {
    throw new AppError("400", "Invalid userName");
  }

  if(status === "pending") {
    throw new AppError("400", "Kindly wait untill we verify your account")
  }

  if(status === "rejected"){
    let text = remarks.reduce((acc,value) => acc + value);
    throw new AppError("400", `You are rejected because of ${text}`);
  }

  // comparing userEntered password and encrypted password n our db 
  const isPasswordCorrect = await bcrypt.compare(password,user.encryptedPassword);

  if(!isPasswordCorrect){
    throw new AppError("400", "Invalid Password for the Entered UserName");
  } 

  const {id} = user;

  const token = signToken(id);
  const refreshToken = signToken(id);

  // TODO store token in sepearte table
  
  return [token,refreshToken];

};


exports.verifyUser = async (token) => {
  console.log("Verifying user");
  
  //check if the token expired
  try {
    var decoded = await jwt.verify(token, process.env.JWT_SECRET);   
  } catch (err) {
    return false;
  }
  const { id, iat: jwtIssuedAt } = decoded;

  //get user details
  const user = await User.unscoped().findByPk(id);

  //check if the user still exist in the db
  if (!user) {
    return false;
  }
  return  user;
};
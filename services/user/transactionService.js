const { Op } = require("sequelize");
const jwt = require("jsonwebtoken");
const db = require("../../db/models");
const AppError = require("../../utils/appError");
const { signToken,signRefreshToken } = require("../common/jwtToken");
const { User } = db;
const bcrypt = require("bcryptjs");
const validator = require("validator");
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.AMOUNT_KEY);


exports.createTransaction = async (benName,benAccountNumber,benIfscNumber,amount,type,userId) => {
 
  const user = await User.findByPk(userId);

  // all the actual logic will be handled by beforeSave callback in the transaction model
  const newTransaction = await user.createTransaction({
    benName,
    benAccountNumber,
    benIfscNumber,
    encryptedAmount: amount,
    type,
    status: "success" ,
    encryptedBalance: user.encryptedAmount
  });

  // since we cannot throw Errors from model
  if (newTransaction.status === "failed")
    throw new AppError("400", "No sufficient balance to debit"); 

}
const { Op } = require("sequelize");
const jwt = require("jsonwebtoken");
const db = require("../../db/models");
const AppError = require("../../utils/appError");
const { signToken,signRefreshToken } = require("../common/jwtToken");
const { User,Transaction } = db;
const bcrypt = require("bcryptjs");
const validator = require("validator");
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.AMOUNT_KEY);
const moment = require("moment");
var PdfPrinter = require("pdfmake");
var fs = require("fs");
const path = require("path");
const awsS3 = require("../common/awsS3");
const s3 = new awsS3();
var fonts = {
  Roboto: {
    normal: "node_modules/roboto-font/fonts/Roboto/roboto-regular-webfont.ttf",
    bold: "node_modules/roboto-font/fonts/Roboto/roboto-bold-webfont.ttf",
    italics: "node_modules/roboto-font/fonts/Roboto/roboto-italic-webfont.ttf",
    bolditalics:
      "node_modules/roboto-font/fonts/Roboto/roboto-bolditalic-webfont.ttf",
  },
};


var printer = new PdfPrinter(fonts);


const buildPdf = async (fromDate,toDate,userTransactions,user) => {
  const userName = user.name;
  const accountNumber = user.accountNumber;
  const currentBalance =  cryptr.decrypt(user.encryptedAmount);
  fromDate = moment(fromDate).format("DD MM YYYY");
  toDate = moment(toDate).format("DD MM YYYY");

  let tableBody = [];
  tableBody.push([
     {text: "Transaction Date",style: [ 'itemsHeaderCenter', 'center']},
     {text: "status",style: [ 'itemsHeaderCenter', 'center']},
     {text: 'Ben Name',style: [ 'itemsHeaderCenter', 'center']},
     {text: 'Ben Account Number',style: [ 'itemsHeaderCenter', 'center']},
     {text: "Amount",style: [ 'itemsHeaderCenter', 'center']},
     {text: "Type",style: [ 'itemsHeaderCenter', 'center']},
     {text: "Balance",style: [ 'itemsHeaderCenter', 'center']},     
  ]);
  userTransactions.map(transaction => {
    let {created_at,status,benName,benAccountNumber,encryptedAmount,type,encryptedBalance} = transaction;
    created_at = moment(created_at).format('DD MM YYYY');
    encryptedAmount = cryptr.decrypt(encryptedAmount);
    encryptedBalance = cryptr.decrypt(encryptedBalance);
    tableBody.push([created_at,status,benName,benAccountNumber,encryptedAmount,type,encryptedBalance]);
  });


  var dd = {
    content: [
      {
          columns:[
              {
                   text: 'Account Statement', 
                      style: 'title',
                      width: '*'
              },
            ],
      },
      {
            columns:[
              {
                   text: accountNumber, 
                     style: 'subTitle',
                     width: 100
              },
              {
                   text: userName, 
                      style: 'subTitle',
                      width: '*'
              },
              
            ]
      },
      '\n',
      {
            columns:[
              {
                   text: 'From Date : ', 
                     style: 'subsubTitle',
                     width: 60
              },
              {
                   text: fromDate, 
                      style: 'subsubTitle',
                      width: 65
              },
              
              {
                   text: '|    To Date : ', 
                     style: 'subsubTitle',
                     width: 60
              },
              {
                   text: toDate, 
                      style: 'subsubTitle',
                      width: 65
              },
              
            ]
      },
      '\n',
      {
            columns:[
              {
                   text: 'Account Balance : ', 
                     style: 'subsubTitle',
                     width: 90
              },
              {
                   text: currentBalance, 
                      style: 'subsubTitle',
                      width: 65
              },
              
            ]
      },
      
      '\n\n',
      {
        style: 'table',
        table: {
          widths: [70, 50, 80,'*',45, 60,60],
          body: tableBody
        }
      },
      
    ],
    
    styles:{
        title: {
        fontSize: 16,
        bold: true,
        alignment:'left',
        margin:[0,0,30,15]
      },
      
      subTitle:{
          fontSize: 10,
          bold: true,
      },
      subsubTitle:{
          fontSize: 10,
      },
      itemsHeaderCenter: {
          fontSize: 12,
          bold: true
      },
      
    }
    
  };

  var pdfDoc = await printer.createPdfKitDocument(dd);
  let fileName = `${Date.now()}_report.pdf`;
  pdfDoc
    .pipe(fs.createWriteStream("tmp/" + fileName))
    .on("finish", function () {
      const invoiceDoc = {
        key: fileName,
        body: "tmp/" + fileName,
      };
      s3.doUpload(invoiceDoc);
    });
  pdfDoc.end();

  user.createReport({
    docKey: fileName,
    name: fileName
  })
}



exports.createReport = async (fromDate,toDate,userId) => {
  const user = await User.findByPk(userId);
  const userTransactions = await Transaction.findAll({
    where: {
      user_id: userId,
      created_at: {
        [Op.between]: [moment(fromDate),moment(toDate)]
      }
    }
  })
  buildPdf(fromDate,toDate,userTransactions,user);

  return userTransactions;


}

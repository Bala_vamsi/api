const { Op } = require("sequelize");
const jwt = require("jsonwebtoken");
const db = require("../../db/models");
const AppError = require("../../utils/appError");
const { signToken,signRefreshToken } = require("../common/jwtToken");
const { User } = db;
const bcrypt = require("bcryptjs");
const validator = require("validator");


const validateFields = async (accountNumber,ifscCode,email,name,userName,password,user) => {
  let remarks = [];

  if (!validator.isNumeric(accountNumber)) {
    remarks.push("Not a valid Account Number");
  }

  if (!validator.isEmail(email)) {
    remarks.push("Not a valid Email address");
  }

  if (!validator.isAlpha(name)) {
    remarks.push("Name should only contain alphabets");
  }

  if (!validator.isAlphanumeric(ifscCode)) {
    remarks.push("Not a valid IFSC Code");
  }

  user.remarks = remarks;

  if(remarks.length == 0) {
    user.registrationStatus = "verified";
  }else{
    user.registrationStatus = "rejected";
  }

  user.save();
};


exports.SignUpUser = async (credentials) => {
  
  const { accountNumber,ifscCode,email,name,userName,password } = credentials;

  try {
    const user = await User.build({
      accountNumber,
      ifscCode,
      email,
      name,
      userName,
      encryptedPassword: password,
    });
    if (user) {
      await user.save();
    }

    // runs in background (i.e async way)
    validateFields(accountNumber,ifscCode,email,name,userName,password,user);

    const token = signToken(user.id);
    const refreshToken = signToken(user.id);

    // TODO store token in sepearte table
  
    return [token,refreshToken];
  } catch (err) {
    console.log("err",err);
    throw new AppError("400", err.errors[0].message || "Cannot create user");
  }

}
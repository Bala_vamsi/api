const aws = require("aws-sdk");
const fs = require("fs");

class awsS3 {
  constructor() {
    this.s3 = new aws.S3({
      accessKeyId: process.env.AMAZON_S3_ACCESS_KEY,
      secretAccessKey: process.env.AMAZON_S3_SECRET_ACCESS_KEY,
    });

    this.uploadParam = {
      Bucket: "npk-product",
      Key: "", // pass key
      Body: null, // pass file body
    };
  }

  async doUpload({ key, body }) {
    const readStream = fs.createReadStream(body);
    this.uploadParam.Key = key;
    this.uploadParam.Body = readStream;

    try {
      const data = await this.s3.upload(this.uploadParam).promise();
      console.log("File uploaded successfully! -> keyname = " + key);
      //once uploaded delete the file in the tmp folder
      fs.unlink(body, (err) => {
        if (err) {
          console.log("Error" + err);
          return;
        }
        console.log(`File ${body} deleted successfully`);
      });
      return key;
    } catch (err) {
      console.log("Error" + err);
    }
  }
  async getFileObject(key) {
    try {
      this.uploadParam.Key = key;
      var params = {
        Bucket: this.uploadParam.Bucket,
        Key: key,
      };
      const data = await this.s3.getObject(params).promise();
      return data;
    } catch (err) {
      console.log("Error" + err);
    }
  }
}

module.exports = awsS3;

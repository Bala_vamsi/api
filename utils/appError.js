class AppError extends Error {
  constructor(statusCode, message) {
    super(message);
    this.errorCode = statusCode || '500';
    this.status = (statusCode.startsWith('4')) ? 'fail' : 'error';
  }
}

module.exports = AppError;

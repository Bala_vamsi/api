const errorHandler = (err, req, res, next) => {
  console.log("err",err);
  res.status(err.errorCode).json({
    status: err.status,
    message: err.message,
  });
};

module.exports = errorHandler;

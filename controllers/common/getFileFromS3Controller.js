const awsS3 = require("../../services/common/awsS3");
const aws = require("aws-sdk");
const fs = require("fs");

aws.config.update({ region: "us-east-2" });


exports.getFileFromS3 = async (req, res, next) => {
  const s3 = new awsS3();
  const { key } = req.params;
 
  const data = await s3.getFileObject(key);

  if (key.split(".").pop() == "pdf") {
    // To display Pdf in Browser
    res.contentType("application/pdf");
    res.send(data.Body);
  } 
};

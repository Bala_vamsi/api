const AppError = require("../../utils/appError");
const transactionService = require("../../services/user/transactionService");
const validator = require("validator");



exports.createTransaction = async (req, res, next) => {
  const {benName,benAccountNumber,benIfscNumber,amount,type} = req.body;
  const { userId } = req;
  try{
    // checking whether all the required fields are entered or not 
    
    for (const [key, value] of Object.entries(req.body)) {
      if(["benName","benAccountNumber","benIfscNumber","amount","type"].includes(key)){
        if(!value) throw new AppError("400", `${key} must be entered`);
      }
    }

    await transactionService.createTransaction(benName,benAccountNumber,benIfscNumber,amount,type,userId);
    
    //if all good send response
     res.status(200).json({
      status: "success",
      data:"Transaction created Successfully"
    });
  }catch(err){
    next(err);
  }
}
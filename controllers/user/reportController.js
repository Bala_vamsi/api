const AppError = require("../../utils/appError");
const reportService = require("../../services/user/reportService");
const validator = require("validator");


exports.createReport = async (req, res, next) => {
  const {fromDate,toDate} = req.body;
  const { userId } = req;
  try{
    // checking whether all the required fields are entered or not 
    
    if(!fromDate) throw new AppError("400", "fromDate is required");

    if(!toDate) throw new AppError("400", "toDate is required");


    const userTransactions = await reportService.createReport(fromDate,toDate,userId);
    
    //if all good send response
     res.status(200).json({
      status: "success",
      data: userTransactions
    });
  }catch(err){
    next(err);
  }
}
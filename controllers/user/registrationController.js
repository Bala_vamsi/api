const registrationService = require("../../services/user/registrationService");
const AppError = require("../../utils/appError");

//SignUp
exports.signup = async (req, res, next) => {
  const { accountNumber, ifscCode, email, name, userName, password } = req.body;

  try {
    if (!userName) throw new AppError("400", "userName must be entered");

    if (!password) throw new AppError("400", "password must be entered");

    if (!email) throw new AppError("400", "Email must be entered");

    if (!name) throw new AppError("400", "Name must be entered");

    let [token, refreshToken] = await registrationService.SignUpUser({
      accountNumber, 
      ifscCode,
      email,
      name,
      userName,
      password,
    });

    //if all good send response
    res.status(200).json({
      status: "success",
      data: {
        token,
        refreshToken,
      },
    });
  } catch (err) {
    next(err);
  }
};

const authService = require("../../services/user/authService");
const AppError = require("../../utils/appError");

//LOGIN
exports.login = async (req, res, next) => {
  const { userName,password } = req.body;

  try {
    if(!userName){
      throw new AppError("400", "userName must be entered");
    }

    if(!password){
      throw new AppError("400", "password must be entered");
    }

    const [token,refreshToken] = await authService.loginUser({ userName,password });
  
 
    //if all good send response
    res.status(200).json({
      status: "success",
      data: {
        token,
        refreshToken
      },
    });
  } catch (err) {
    next(err);
  }
};


//VERIFY USER
exports.verifyUser = async (req, res, next) => {
  const { authorization } = req.headers;
  if (authorization) {
    const token = await authorization.split(" ")[1];
    const user = await authService.verifyUser(token);
    if (user) {
      const {registrationStatus,remarks} = user;
      if(registrationStatus === "pending") {
        next(
          new AppError("400", "Kindly wait untill we verify your account")
        );
      }
      if(registrationStatus === "rejected"){
         let text = remarks.reduce((acc,value) => acc + value);
        next(
          new AppError("400", `You are rejected because of ${text}`)
        );
      }
      req.userId = user.id;
      next();
    } else {
      next(
        new AppError(
          "401",
          "Invalid token, kindly login to access this resource"
        )
      );
    }
  } else {
    next(
      new AppError("401", "Invalid token, kindly login to access this resource")
    );
  }
};
'use strict';
const { Model } = require('sequelize');
const bcrypt = require("bcryptjs");
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.AMOUNT_KEY);


module.exports = (sequelize, DataTypes) => {
  class Transaction extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Transaction.belongsTo(models.User, { foreignKey: "user_id" });
    }
  };

  Transaction.init({
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
    },
    benName: {
      type: DataTypes.STRING,
      allowNull: false,
      field: "ben_name",
    },
    benAccountNumber: {
      type: DataTypes.STRING,
      allowNull: false,
      field: "ben_account_number",
    },
    benIfscNumber: {
      type: DataTypes.STRING,
      allowNull: false,
      field: "ben_ifsc_code",
    },
    encryptedAmount: {
      type: DataTypes.STRING,
      allowNull: false,
      field: "encrypted_amount",
    },
    encryptedBalance: {
      type: DataTypes.STRING,
      allowNull: false,
      field: "encrypted_balance",
    },
    type:{
      type: DataTypes.ENUM(
        "debit",
        "credit",
      ),
      defaultValue: "debit",
      allowNull: false,
      field: "type"
    },      
    status: {
      type: DataTypes.ENUM(
        "pending",
        "success",
        "failed"
      ),
      defaultValue: "pending",
      allowNull: false,
      field: "status"
    },
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "users",
        key: "id",
      },
    },
  }, {
    sequelize,
    modelName: 'Transaction',
    underscored: true,
  });

  Transaction.addHook("beforeSave", async (transaction, options) => {
    
    if (options.fields.includes("type") && options.fields.includes("encryptedAmount") && options.fields.includes("status")) {
      // get the user
      const user = await transaction.getUser();
      
      if(transaction.type === "credit" ) {
        user.encryptedAmount = parseFloat(cryptr.decrypt(user.encryptedAmount)) + parseFloat(transaction.encryptedAmount);
        transaction.encryptedBalance = cryptr.encrypt(user.encryptedAmount);
      }else{
        // current account balance in userAccount
        const amountInAcc =  parseFloat(cryptr.decrypt(user.encryptedAmount));

        // checking whether there is sufficient balance to debit amount from account
        if(amountInAcc < parseFloat(transaction.encryptedAmount)){
          transaction.status = "failed";
          transaction.encryptedBalance = user.encryptedAmount;
        }else{
          user.encryptedAmount = amountInAcc - parseFloat(transaction.encryptedAmount);
          transaction.encryptedBalance = cryptr.encrypt(user.encryptedAmount);
        }
      }      
      await user.save();
    }

    // encrypting transaction amount
    if (options.fields.includes("encryptedAmount")) {      
      transaction.encryptedAmount = cryptr.encrypt(transaction.encryptedAmount || 0);
    }
  });


  return Transaction;
};
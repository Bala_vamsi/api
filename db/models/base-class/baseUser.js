"use strict";
const { Model } = require("sequelize");
const bcrypt = require("bcryptjs");
const crypto = require("crypto");

class BaseUser extends Model {
  static getHashedToken(token) {
    return crypto.createHash("sha256").update(token).digest("hex");
  }

  async verifyPassword(password) {
    return await bcrypt.compare(password, this.encryptedPassword);
  }

  generateToken(action) {
    //create token and set token expiry as 10m from now
    const token = crypto.randomBytes(32).toString("hex");
    const tokenExpiry = Date.now() + 10 * 60 * 1000;

    if (action === "passwordReset") {
      this.passwordResetToken = BaseUser.getHashedToken(token);
      this.passwordResetTokenExpiresIn = tokenExpiry;
    }
    return token;
  }

  hasPasswordChangedAfter(jwtIssuedAt) {
    if (this.passwordModifiedAt) {
      //Converting from ms to s as jwt will be in seconds
      const changedTimestamp = Number(this.passwordModifiedAt.getTime()) / 1000;
      return changedTimestamp > jwtIssuedAt;
    }
    return false;
  }
}

module.exports = BaseUser;

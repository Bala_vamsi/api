'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Report extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Report.belongsTo(models.User, { foreignKey: "user_id" });
    }
  };
  Report.init({
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: "name",
    },
    docKey: {
      type: DataTypes.STRING,
      allowNull: false,
      field: "doc_key",
    },
    user_id: {
      type: DataTypes.INTEGER,
      references: {
        model: "users",
        key: "id",
      },
    },
  }, {
    sequelize,
    modelName: 'Report',
  });
  return Report;
};
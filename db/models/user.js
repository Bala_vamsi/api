"use strict";
const bcrypt = require("bcryptjs");
const Cryptr = require('cryptr');
const cryptr = new Cryptr(process.env.AMOUNT_KEY);
const { BaseUser } = require("./base-class");
const AppError = require("../../utils/appError");

module.exports = (sequelize, DataTypes) => {
  class User extends BaseUser {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
    */

    static associate(models) {
      User.hasMany(models.Transaction, { foreignKey: "user_id" })
      User.hasMany(models.Report, { foreignKey: "user_id" })
    }
  }
  User.init(
    {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
      },
      accountNumber: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        field: "account_number"
      },
      ifscCode: {
        type: DataTypes.STRING,
        allowNull: false,
        field: "ifsc_code",
      },
      userName: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        field: "user_name",
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "name",
      },
      encryptedPassword: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "encrypted_password",
      },
      encryptedAmount: {
        type: DataTypes.STRING,
        allowNull: true,
        field: "encrypted_amount",
      },
      registrationStatus: {
        type: DataTypes.ENUM(
          "pending",
          "verified",
          "rejected"
        ),
        defaultValue: "pending",
        allowNull: false,
        field: "registration_status",
      },
      remarks:{
        type: DataTypes.ARRAY(DataTypes.STRING),
        field: "remarks",
      },
    },
    {
      sequelize,
      modelName: "User",
      underscored: true,
    }
  );

  User.addHook("beforeSave", async (user, options) => {
    
    if (options.fields.includes("encryptedPassword")) {
      user.encryptedPassword = await bcrypt.hash(user.encryptedPassword, 12);
    }

    if (options.fields.includes("encryptedAmount")) {      
      user.encryptedAmount = cryptr.encrypt(user.encryptedAmount || 0);
    }

    //conerting user email to lower case
    if (options.fields.includes("email")) {
      user.email = user.email.toLowerCase();
    }
  });
  return User;
};

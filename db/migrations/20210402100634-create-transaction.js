'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('transactions', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      benName: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "ben_name",
      },
      benAccountNumber: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "ben_account_number",
      },
      benIfscNumber: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "ben_ifsc_code",
      },
      encryptedAmount: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "encrypted_amount",
      },
      encryptedBalance: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "encrypted_balance",
      },
      type:{
        type: Sequelize.ENUM(
          "debit",
          "credit",
        ),
        defaultValue: "debit",
        allowNull: false,
        field: "type"
      },      
      status: {
        type: Sequelize.ENUM(
          "pending",
          "success",
          "failed"
        ),
        defaultValue: "pending",
        allowNull: false,
        field: "status"
      },
      user_id: {
        type: Sequelize.INTEGER,
        references: {
          model: "users",
          key: "id",
        },
      },
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Transactions');
  }
};
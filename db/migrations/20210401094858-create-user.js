'use strict';
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable('users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      accountNumber: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        field: "account_number"
      },
      ifscCode: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "ifsc_code",
      },
      userName: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
        field: "user_name",
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true,
      },
      name: {
        type: Sequelize.STRING,
        allowNull: true,
        field: "name",
      },
      encryptedPassword: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "encrypted_password",
      },
      encryptedAmount: {
        type: Sequelize.STRING,
        allowNull: false,
        field: "encrypted_amount",
      },
      registrationStatus: {
        type: Sequelize.ENUM(
          "pending",
          "verified",
          "rejected"
        ),
        defaultValue: "pending",
        allowNull: false,
        field: "registration_status"
      },
      remarks: Sequelize.ARRAY(Sequelize.STRING),
      created_at: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updated_at: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable('Users');
  }
};
const express = require("express");
const { verifyUser } = require("../controllers/user/authController");
const router = express.Router();
const {createTransaction} = require("../controllers/user/transactionController")


router.route("/").post(verifyUser, createTransaction);
module.exports = router;


const express = require("express");
const { verifyUser } = require("../controllers/user/authController");
const router = express.Router();
const {createReport} = require("../controllers/user/reportController")


router.route("/").post(verifyUser, createReport);
module.exports = router;
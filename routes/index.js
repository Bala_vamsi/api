const transactionRoutes = require("./transactionRoutes");
const reportRoutes = require("./reportRoutes");

module.exports = {
  transactionRoutes,
  reportRoutes
}

const dotenv = require("dotenv");
dotenv.config({ path: "./config.env" });
const app = require("./app");
const PORT = process.env.PORT || 3003;
const db = require("./db/models");
const { User } = db;


app.listen(PORT, async () => {
  console.log(`Server running in port ${PORT}`);
  // User.sync({ force: true })
});
